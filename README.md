Task: Use NodeJS to implement web-server which hosts and serves
files.

Making API calls, a user is able to:
- create a file,
- get a list of all files from a folder,
- read a content of a file

Tools used: Express.js, Nodejs file system, Swagger UI, nodemon, git, morgan, postman
