const express = require('express');
const fs = require('fs');
const app = express();
const morgan = require('morgan');
const cors = require('cors');
const path = require('path');
const filesFolder = path.join(__dirname, 'files');

try {
    if (!fs.existsSync(filesFolder)) {
        console.log('folder created')
        fs.mkdirSync(filesFolder);
    }
} catch (err) {
    throw err;
}

const getFilePath = (filename) => path.join(filesFolder, filename);

app.use(cors());
app.use(morgan('tiny'));
app.use(express.json());

function createdDate(file) {  
    const { birthtime } = fs.statSync(file)
    return birthtime
}

const regex = new RegExp('^.*\.(log|txt|json|yaml|xml|js)$');

app.post('/api/files', function createFile(req, res) {
    const { filename, content } = req.body;
    if (!content) {
        return res.status(400).json({message: "Please specify 'content' parameter"});
    } else if (!filename) {
        return res.status(400).json({message: "Please specify 'filename' parameter"});
    } else if (!regex.test(filename)) {
        return res.status(400).json({message: "Incorrect extension"});
    } else {
        fs.open(getFilePath(filename), 'r', (err) => {
            if(err) {
                fs.writeFile(getFilePath(filename), content, function(err) {
                    if(err) throw err;
                    return res.status(200).json({message: "File created successfully"})
                }); 
            } else {
                return res.status(500).json({message: "Server error"});
            }
        })
    }           
})

app.get('/api/files', function getFiles(req, res) {
    if(!fs) {
        return res.status(500).json({message: "Server error"})
    } else {
        fs.readdir(filesFolder, (err, files) => {
            if(err) {
                return res.status(400).json({message: "Client error"}) 
            } else {
                return res.status(200).json({
                    message: "Success",
                    files
                })
            }
        })
    }
})

app.get('/api/files/:filename', function getFile(req, res) {
    const {filename} = req.params;
    fs.readdir(filesFolder, (err, files) => {
        if(err) {
            return res.status(500).json({message: "Server error"}) 
        } else {
            if (!files.find(file => file === filename)) {
                return res.status(400).json({message : `No file with '${filename}' filename found`})
            } else {
                const data = fs.readFileSync(getFilePath(filename), 'utf8')
                return res.status(200).json({
                    message: "Success",
                    filename: filename,
                    content: data,
                    extension: filename.substring(filename.lastIndexOf('.') + 1, filename.length),
                    uploadedDate: createdDate(getFilePath(filename))
                }) 
            }    
        }
    })

})

app.patch('/api/files', function modifyContent(req, res) {
    const {filename, newContent } = req.body;
    if (!newContent) {
        return res.status(400).json({message: "Please specify 'newContent' parameter"});
    } else if (!filename) {
        return res.status(400).json({message: "Please specify 'filename' parameter"});
    } else {
       fs.readdir(filesFolder, (err, files) => {
            if(err) {
                return res.status(500).json({message: "Server error"}) 
            } else {
                if (!files.find(file => file === filename)) {
                    return res.status(400).json({message : `No file with '${filename}' filename found`})
                } else {
                    fs.writeFile(getFilePath(filename), newContent, function(err) {
                        if(err) throw err;
                        return res.status(200).json({message: "File edited successfully"})
                    });
                }    
            }
        }) 
    }  
})

app.delete('/api/files/:filename', function deleteFile(req, res) {
    const { filename } = req.params;
    fs.readdir(filesFolder, (err, files) => {
        if(err) {
            return res.status(500).json({message: "Server error"}) 
        } else {
            if (!files.find(file => file === filename)) {
                return res.status(400).json({message : `No file with '${filename}' filename found`})
            } else {
                fs.unlink(getFilePath(filename), err => {
                    if(err) throw err;
                    return res.status(200).json({message: `${filename} deleted successfully`})
                }) 
            }    
        }
    })   
})

app.listen(8080, () => {
    console.log("LISTENING ON PORT 8080")
})